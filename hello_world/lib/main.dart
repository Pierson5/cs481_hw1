//Name: Jeremiah Pierson
//9/10/2020

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hello World',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to flutter'),
        ),
        body: Center(
          child: Text('''Name: Jeremiah Pierson
                         Quote: “By all means, marry. If you get a good wife, you will be happy. If you get a bad one, you will be a philosopher.” - Socrates
                         Graduation Date: 1/1/2021''',maxLines: 20, style: TextStyle(fontSize: 16.0 ,fontWeight:FontWeight.bold,color: Colors.black)
          ),
      ),
      ),
    );
  }
}
